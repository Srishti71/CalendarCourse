package com.example.shristi.calendarcoursecodify;

import android.Manifest;
import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SettingsActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Set the Back button on the action bar.
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Load the calendar when the app is installed (for the first time when the app runs)
        SharedPreferences sharedPreferences =   PreferenceManager.getDefaultSharedPreferences(this);//

        boolean firstrun = sharedPreferences.getBoolean("FirstRun1", true);

try {

    SQLiteDatabase sqLiteDatabase = this.openOrCreateDatabase("MyCalendarDb", MODE_PRIVATE, null);
    sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS New_MyCalendar_Table (" + "CalendarName VARCHAR," + "CalendarId VARCHAR)");


    //Permission to get the data from Calendar
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.READ_CALENDAR}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

    }
    Log.i("Firstrun1", firstrun+"");

    if(firstrun ) {

        sharedPreferences.edit().putBoolean("FirstRun1", false).commit();
        Cursor cursor;

        if (android.os.Build.VERSION.SDK_INT <= 7) {
            cursor = getContentResolver().query(Uri.parse("content://calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);
        } else if (android.os.Build.VERSION.SDK_INT <= 14) {
            cursor = getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);
        } else {
            cursor = getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "calendar_displayName"}, null, null, null);
        }

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {

                //Saving data to the List elements so that it can be sent to the preference fragment
                MainActivity.calendarIdsList.add(cursor.getInt(0) + "");

                MainActivity.calendarNamesList.add(cursor.getString(1));

                sqLiteDatabase.execSQL("INSERT INTO New_MyCalendar_Table(CalendarName,CalendarId) Values('" + cursor.getString(1) + "', '" + cursor.getInt(0) + "')");


                cursor.moveToNext();

            }
            Log.i("Cursor for", cursor.getString(1) + "insert data");
        }


    }
/*
* This code is mainly for the sake of running the app. Because of the shared preferences, I had to uninstall my app to run the code.
*
* */
      if(MainActivity.calendarNamesList.size() <=  0) {
        Cursor cursor1 = sqLiteDatabase.query("New_MyCalendar_Table",new String[]{"CalendarName", "CalendarId"},null,null,null,null, null,null);
        Log.i("Cursor1:", cursor1.getCount() + "get data");
        if (cursor1.getCount() > 0) {
            cursor1.moveToFirst();
            for (int j = 0; j < cursor1.getCount(); j++) {
                MainActivity.calendarIdsList.add(cursor1.getString(1));
                MainActivity.calendarNamesList.add(cursor1.getString(0));
                cursor1.moveToNext();
            }

        }
    }

}

       catch(Exception e){
           e.printStackTrace();
       }

        //replace the activity with the preference Fragment
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SelectCalendarsFragment())
                .commit();



    }

    //Used for the back button in the action bar.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Setting Destroy", "called");

    }
}
