package com.example.shristi.calendarcoursecodify;

import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.audiofx.BassBoost;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Shristi on 10/28/2017. This class creates the ListPreferences with the radio button in the ListPreference element
 */

public class SelectCalendarsFragment extends PreferenceFragment {

    /*
    Convert the calendar Id and calendar Name which is saved in a list to array because ListPreference accepts array of type CharSequence

*/

    CharSequence[] calendarNamesArray = MainActivity.calendarNamesList.
            toArray(new CharSequence[MainActivity.calendarNamesList.size()]);

    String selectedCalendar= "1";
    CharSequence[] calendarIdsArray= new CharSequence[calendarNamesArray.length];

    //Getting the events
    String[] EVENT_PROJECTION = new String[]{
            CalendarContract.Events.TITLE, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND

    };

    //To get the index value of the item that was selected as a Preferred calendar

    int selectedIndex;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        Everytime this page is called the calendar_preferences.xml is loaded
        Note: calendar_preferences.xml is empty
         */
        addPreferencesFromResource(R.xml.calendar_preferences);


        /*
        In order to get the state of previous state. Shared Preferences is used here
         */

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        PreferenceScreen preferenceScreen = getPreferenceScreen();
        PreferenceCategory preferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
        final ListPreference listCalendarPreference = new ListPreference(preferenceScreen.getContext());
        preferenceCategory.setTitle("Settings");
        preferenceScreen.addPreference(preferenceCategory);
        listCalendarPreference.setTitle("Sync Calendar");
        listCalendarPreference.setEntries(calendarNamesArray);
        listCalendarPreference.setEntryValues(calendarIdsArray);
        listCalendarPreference.setValue(calendarIdsArray + "");
        preferenceScreen.addPreference(listCalendarPreference);


        for(int i=0;i<calendarNamesArray.length;i++)
            calendarIdsArray[i] = (i+1)+"";

        Log.i("Previously Selected", sharedPreferences.getInt("SelectedIndex",0)+"");
        if(sharedPreferences.getInt("SelectedIndex",0) != 0 && calendarNamesArray.length>0){
            Log.i("Inside", "if statement");
            try{
                listCalendarPreference.setValueIndex(sharedPreferences.getInt("SelectedIndex",0)-1);
                selectedCalendar = listCalendarPreference.getEntry()+"";

            }
           catch(Exception e){
               Toast.makeText(getActivity().getApplicationContext(), "Problem with launching. Please try reinstalling the app", Toast.LENGTH_LONG).show();
               e.printStackTrace();
           }
        }

        if(sharedPreferences.getInt("SelectedIndex", 0) != 0){
            Cursor cursorForEvent = null;
            ContentResolver contentResolver = getActivity().getContentResolver();

            Uri uri = Uri.parse("content://com.android.calendar/events");
            String selection = "(" + CalendarContract.Calendars.CALENDAR_DISPLAY_NAME + "= ?)" ;
            Log.i("Here we are in ", "getting events");
            String[] selectionArgs = new String[]{selectedCalendar};
            cursorForEvent = contentResolver.query(uri,EVENT_PROJECTION, selection, selectionArgs, null);

            Log.i("Lets see events", cursorForEvent.getCount()+"");
            if(cursorForEvent.getCount()>0){
                cursorForEvent.moveToFirst();

                for(int i=0; i<cursorForEvent.getCount();i++)
                {

                    if((cursorForEvent.getString(0))!=null && cursorForEvent.getString(1)!= null && cursorForEvent.getString(2)!=null){
                        //because there were some data in my calendar with null endtime/start time/ title
                        Log.i("Event Name",""+cursorForEvent.getString(0));
                        Log.i("Start Time",""+getDate(Long.parseLong(cursorForEvent.getString(1))));
                        Log.i("End Time",""+getDate(Long.parseLong(cursorForEvent.getString(2))));

                    }
                    cursorForEvent.moveToNext();
                }

            }

        }

        //Save the state if changed
        Preference.OnPreferenceChangeListener listCalendarPreference1 = new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    String getSelectedIndex = newValue.toString();
                    Log.i("getSelectedIndex", getSelectedIndex);
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    sharedPreferences.edit().putInt("SelectedIndex", Integer.parseInt(getSelectedIndex)).commit();



                    return true;
                }
            };

        listCalendarPreference.setOnPreferenceChangeListener(listCalendarPreference1);

       }
    public static String getDate(long milliSeconds) {
        //Change the format to dd/mm/yyy
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }



    }




