package com.example.shristi.calendarcoursecodify;


import android.Manifest;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.provider.CalendarContract;

import android.support.v4.app.ActivityCompat;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashSet;

import java.util.List;
import java.util.Set;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class MainActivity extends AppCompatActivity {


    static List<String> calendarNamesList = new ArrayList<String>();
    static List<CharSequence> calendarIdsList = new ArrayList<CharSequence>();


    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button gotoSettingsButton = (Button) findViewById(R.id.button);
        Log.i("Main Activity", "called");
        gotoSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);

            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_CALENDAR}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("MainActivity Destroy", "called");

    }

    @Override
        public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_READ_CONTACTS:
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PERMISSION_GRANTED) {

                        Log.i("Permission Granted","Permission Granted");


                    } else {
                            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CALENDAR))
                            {
                                new AlertDialog.Builder(this)
                                        .setTitle("Read Contact Permission")
                                        .setMessage("You need to grant read calendar Permission").show();
                            }
                            else {


                                 new AlertDialog.Builder(this)
                                         .setTitle("Read Contact Permission Denied")
                                         .setMessage("You need to grant read calendar Permission Denied").show();
                            }

                    }
                    break;

            }
        }


}
